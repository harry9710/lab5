/* Task 1.
1.     Write a program using iteration for calculating a factorial:  take the input number from user
2.     Rewrite it using recursion */

package lab5;
import java.util.Scanner;

public class Task1 {
	public static void main(String[] args) {
		//take the input number
		Scanner input = new Scanner(System.in);
		System.out.println("Input a number:");
		int a = input.nextInt();
		System.out.printf("Factorial of %d by using iteration: %d\n", a, iter(a));
		System.out.printf("Factorial of %d by using recursion: %d", a, recur(a));	
	}
	
	//method getting factorial by iteration
	public static int iter(int n) {
		int result = 1;
		for(int i = 1; i <= n; i++) {
			result*=i;
		}
		return result;
	}
	
	//method getting factorial by recursion
	public static int recur(int n) {
		if (n <= 1) 
			return 1;	
		else 		
			return n*recur(n-1);		
	}
}

