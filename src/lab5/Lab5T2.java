/*Task 2.
Write a programme for checking whether a given String (input taken from user) is a palindrome. Use Stack structure to solve the task.*/

package lab5;
import java.util.*;

public class Lab5T2 {

	public static void main(String[] args) {
		System.out.println("Input a word:");
		Scanner input = new Scanner(System.in);
		String original = input.nextLine();
		String reverseOriginal = "";
		Stack stack = new Stack();
		
		//create a stack including all characters of the original string
		for (int i = 0; i < original.length(); i++) 
			stack.push(original.charAt(i));
			
		//create a string including all characters of the original string in reverse order
		while (!stack.isEmpty()) 
			reverseOriginal += stack.pop();
		
		if (original.equals(reverseOriginal))
			System.out.println("This is a palindrome");
		else
			System.out.println("This isn't a palindrome");
	}

}
